require('mason').setup({
	pip = {
		upgrade_pip = true
	},
	ensure_installed = {
		'pyright'
	}
})
