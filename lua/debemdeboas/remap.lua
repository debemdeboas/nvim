vim.keymap.set('n', '<leader>pv', vim.cmd.Ex)

-- LSP
vim.keymap.set('n', '<C-e>', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<C-q>', vim.diagnostic.setloclist)

-- Terminal
for _, mode in ipairs({'n', 't'}) do
	vim.keymap.set(mode, '<A-h>', function () require('nvterm.terminal').toggle('horizontal') end)
	vim.keymap.set(mode, '<A-v>', function () require('nvterm.terminal').toggle('vertical') end)
	vim.keymap.set(mode, '<A-i>', function () require('nvterm.terminal').toggle('float') end)
end

