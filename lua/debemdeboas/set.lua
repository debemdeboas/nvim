-- set ignorecase, incsearch and hlsearch 
-- set statusbar
-- download 

vim.o.guicursor = ""
vim.o.nu = true

vim.o.hlsearch = true
vim.o.incsearch = true
vim.o.ignorecase = true

vim.wo.number = true

vim.o.mouse = 'a'

vim.o.clipboard = 'unnamedplus'

vim.o.breakindent = true

vim.o.swapfile = false
vim.o.backup = false
vim.o.undodir = os.getenv('HOME') .. '/.vim/undodir'
vim.o.undofile = true

vim.o.smartcase = true
vim.o.smartindent = true
vim.o.wrap = false

vim.wo.signcolumn = 'yes'

vim.o.updatetime = 300
vim.o.timeoutlen = 300

vim.o.completeopt = 'menu,menuone,noselect'

vim.o.termguicolors = true

vim.o.expandtab = true

vim.o.colorcolumn = '80,120'
vim.o.cursorline = true

vim.o.relativenumber = false

vim.o.laststatus = 2
vim.o.showmode = false

vim.o.splitright = true
vim.o.splitbelow = true

