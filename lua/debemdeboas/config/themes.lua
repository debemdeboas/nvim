require('onedarkpro').setup({
	options = {
		transparency = true,
		cursorline = true,
		terminal_colors = false,
		highlight_inactive_windows = true,
	}
})
