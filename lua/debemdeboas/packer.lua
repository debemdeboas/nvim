-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- Git
  use 'tpope/vim-fugitive'
  use 'tpope/vim-rhubarb'

  -- Detect tabstop and shiftwidth automatically
  use 'tpope/vim-sleuth'

  use {
	  'nvim-telescope/telescope.nvim',
	  tag = '0.1.2',
	  requires = { {'nvim-lua/plenary.nvim'} }
  }

  -- Themes
  use ({
	  'rose-pine/neovim',
	  as = 'rose-pine',
  })
  use 'olimorris/onedarkpro.nvim'
  use { 'catppuccin/nvim', as = 'catppuccin' }
  use "EdenEast/nightfox.nvim"

  use 'folke/neodev.nvim'

  -- LSP
  use {
	  'nvim-treesitter/nvim-treesitter',
	  run = ':TSUpdate'
  }

  use 'nvim-treesitter/playground'

  use {
	  'neovim/nvim-lspconfig',
  }

  use 'williamboman/mason.nvim'
  use 'williamboman/mason-lspconfig.nvim'

  use 'j-hui/fidget.nvim'

  -- GitLab Copilot
  use {
	  "git@gitlab.com:gitlab-org/editor-extensions/gitlab.vim.git",
  }

end)

